var isAboveBlock = false;
var scoreDisplay = document.getElementById("score");
var score = 0;
var isJumping = false;
var highScore = localStorage.getItem("highScore") || 0;


function jump() {
    if (isJumping) return;
    isJumping = true;
    var jumpInterval = setInterval(function() {
      var characterTop = parseInt(window.getComputedStyle(character).getPropertyValue("top"));
      if (characterTop > 180 && !character.classList.contains("hover")) {
        character.style.top = (characterTop - 5) + "px";
      } else {
        clearInterval(jumpInterval);
        setTimeout(function() {
          character.classList.add("hover");
          setTimeout(function() {
            character.classList.remove("hover");
            var fallInterval = setInterval(function() {
              var characterBottom = characterTop + parseInt(window.getComputedStyle(character).getPropertyValue("height"));
              if (characterBottom < 300) { // adjust this value to change the fall height
                character.style.top = (characterTop + 5) + "px";
                characterTop += 5;
              } else {
                clearInterval(fallInterval);
                character.classList.remove("animate");
                character.style.top = "300px"; // reset character position after falling
                isJumping = false;
              }
            }, 5);
          }, 50); // adjust this value to change the hover duration
        }, 200); // adjust this value to change the delay before falling
      }
    }, 0);
  }
    document.addEventListener('keydown', function(event) {
        if (event.code == 'Space') {
            jump();
        }
    });
  
  block.addEventListener("animationend", function() {
    // Reposition the block to the right side of the screen
    block.style.left = "100%";
    block.style.top = Math.floor(Math.random() * 300) + "px";
    isAboveBlock = false;
  });
  
  function checkCollision() {
    var characterTop = parseInt(window.getComputedStyle(character).getPropertyValue("top"));
    var characterBottom = characterTop + parseInt(window.getComputedStyle(character).getPropertyValue("height"));
    var characterLeft = parseInt(window.getComputedStyle(character).getPropertyValue("left"));
    var characterRight = characterLeft + parseInt(window.getComputedStyle(character).getPropertyValue("width"));
  
    var blocks = document.querySelectorAll("#block");
    blocks.forEach(function(block) {
      var blockTop = parseInt(window.getComputedStyle(block).getPropertyValue("top"));
      var blockBottom = blockTop + parseInt(window.getComputedStyle(block).getPropertyValue("height"));
      var blockLeft = parseInt(window.getComputedStyle(block).getPropertyValue("left"));
      var blockRight = blockLeft + parseInt(window.getComputedStyle(block).getPropertyValue("width"));
  
      if (characterBottom >= blockTop && characterTop <= blockBottom && characterRight >= blockLeft && characterLeft <= blockRight) {
        endGame();
      }
    });
  }

function endGame() {
  clearInterval(checkDead);
  block.style.animation = "none";
  block.style.display = "none";
  console.log("Game over"); // Add console.log statement
  character.classList.remove("animate");
  document.getElementById("you-lose").style.display = "block";
}
var debounce = false;
var checkDead = setInterval(function(){
  if (!character.classList.contains("animate")) {
    checkCollision();
  }
  var blockLeft = parseInt(window.getComputedStyle(block).getPropertyValue("left"));
  if (blockLeft < 0 && !isAboveBlock) {
    isAboveBlock = true;
    score++;
    if (score > highScore) {
      highScore = score;
      localStorage.setItem("highScore", highScore);
    }
    scoreDisplay.textContent = "Score: " + score + " High Score: " + highScore;
    isScored = true; // set flag to true when the character passes the block
  } else if (blockLeft < -20 && isAboveBlock) {
    isAboveBlock = false;
    if (isScored) {
      isScored = false; // reset flag when the block is reset to the right side
    }
  }
}, 1);

var scoreDisplay = document.getElementById("score");
scoreDisplay.textContent = "Score: " + score +"High Score: " + highScore;
var isScored = false; // flag to false//

function playAudio() {
    var audio = document.getElementById("myAudio");
    audio.play();
  }
  var audio = document.getElementById("myAudio");
  var button = document.querySelector(".button");
  button.addEventListener("click", function() {
      audio.play();
  });

  document.addEventListener("keydown", function(event) {
    if (event.code === "Space") {
      start();
    }
  });